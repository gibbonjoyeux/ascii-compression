/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

"use strict";

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	CHAR				= " `~._^|',-!:}+{=\\/*;[]7oc><i?)(rlt1jsIz3vCuJ%5aYn\"298e0f&L6OS$VGZxTyUhP4wkDFdgqbRpmX@QAEHK#BNWM";

let		ready;
let		d_video;
let		d_canvas;
let		context;
let		ascii_resized;
let		dark_mode;
let		font_size;
let		default_font_size;
let		controls_focused_timer;

let		d_video_player;
let		d_controls;
let		d_time_seek;
let		d_time_elapsed;
let		d_time_duration;
let		d_time_cursor;
let		d_play;
let		d_reverse;
let		d_zoom;
let		d_unzoom;
let		d_reset_zoom;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// TRANSLATION
//////////////////////////////////////////////////

function	draw_characters() {
	let		x, y;
	let		pix_x, pix_y, pix_i;
	let		img_data;
	let		gray;

	/// GET DATA
	img_data = context.getImageData(0, 0, d_canvas.width, d_canvas.height);
	for (y = 0; y < canvas_height; ++y) {
		pix_y = y * d_canvas.width * 4;
		for (x = 0; x < canvas_width; ++x) {
			/// GET PIXEL POSITION
			pix_x = x * 4;
			pix_i = pix_y + pix_x;
			/// GET GRAY
			gray = (img_data.data[pix_i] + img_data.data[pix_i + 1] + img_data.data[pix_i + 2]) / 3;
			if (dark_mode == true) {
				gray = floor(map(gray, 0, 255, 0, CHAR.length - 1, true));
			} else {
				gray = floor(map(gray, 255, 0, 0, CHAR.length - 1, true));
			}
			ascii[y][x] = CHAR[gray];
		}
	}
}

//////////////////////////////////////////////////
/// CONTROLS
//////////////////////////////////////////////////

function	format_time(time) {
	let	minutes;
	let	seconds;

	/// COMPUTE VALUE
	minutes = Math.floor(time / 60);
	seconds = Math.floor(time - minutes * 60);
	/// CONVERT TO STRING
	minutes = ((minutes < 10) ? "0" : "") + minutes;
	seconds = ((seconds < 10) ? "0" : "") + seconds;
	/// RETURN FORMATED STRING
	return (minutes + ":" + seconds);
}

function	setup_buttons() {
	/// LOAD DOM
	d_video_player = document.getElementById("player_video");
	d_controls = document.getElementById("player_controls");
	d_time_seek = document.getElementById("player_controls_seek");
	d_time_elapsed = document.getElementById("time_elapsed");
	d_time_duration = document.getElementById("time_duration");
	d_time_cursor = document.getElementById("time_cursor");
	d_play = document.getElementById("b_play");
	d_reverse = document.getElementById("b_reverse");
	d_zoom = document.getElementById("b_zoom");
	d_unzoom = document.getElementById("b_unzoom");
	d_reset_zoom = document.getElementById("b_reset_zoom");
	/// EVENTS
	d_time_seek.addEventListener("input", function () {
		d_video.currentTime = d_time_seek.value;
	});
	d_time_seek.addEventListener("mousemove", function (e) {
		let	current_time;

		d_time_cursor.style.visibility = "visible";
		current_time = Math.round((e.offsetX / d_time_seek.clientWidth) * parseInt(d_time_seek.max, 10));
		d_time_cursor.textContent = format_time(current_time);
		d_time_cursor.style.left = e.clientX - (d_time_cursor.clientWidth / 2) + "px";
	});
	d_time_seek.addEventListener("mouseup", function () {
		d_time_seek.blur();
	});
	d_time_seek.addEventListener("mouseout", function () {
		d_time_cursor.style.visibility = "hidden";
	});
	d_reverse.addEventListener("click", function() {
		if (dark_mode == true) {
			dark_mode = false;
			document.body.style.background = "white";
			document.body.style.color = "black";
			d_reverse.textContent = "dark";
			d_time_seek.style.filter = "invert(100%)";
			d_controls.setAttribute("class", "player_controls_invert");
			d_reverse.setAttribute("class", "button_invert");
			d_zoom.setAttribute("class", "button_invert");
			d_unzoom.setAttribute("class", "button_invert");
			d_reset_zoom.setAttribute("class", "button_invert");
			d_play.setAttribute("class", "button_invert");
			d_time_cursor.setAttribute("class", "time_cursor_invert");
		} else {
			dark_mode = true;
			document.body.style.background = "black";
			document.body.style.color = "white";
			d_reverse.textContent = "light";
			d_time_seek.style.filter = "invert(0%)";
			d_controls.setAttribute("class", "");
			d_reverse.setAttribute("class", "");
			d_zoom.setAttribute("class", "");
			d_unzoom.setAttribute("class", "");
			d_reset_zoom.setAttribute("class", "");
			d_play.setAttribute("class", "");
			d_time_cursor.setAttribute("class", "");
		}
	});
	d_play.addEventListener("click", function() {
		if (d_video.paused == true) {
			d_video.play();
			d_play.textContent = "||";
		} else {
			d_video.pause();
			d_play.textContent = "->";
		}
	});
	d_unzoom.addEventListener("click", function() {
		font_size = Math.max(font_size - 10, 10);
		d_video_player.style["font-size"] = font_size + "%";
	});
	d_zoom.addEventListener("click", function() {
		font_size = Math.min(font_size + 10, 200);
		d_video_player.style["font-size"] = font_size + "%";
	});
	d_reset_zoom.addEventListener("click", function() {
		font_size = default_font_size;
		d_video_player.style["font-size"] = font_size + "%";
	});
	document.addEventListener("keydown", function(e) {
		/// PAUSE
		if (e.code == "Space") {
			d_play.click();
		/// +5s
		} else if (e.code == "ArrowRight") {
			d_video.currentTime = Math.min(d_video.currentTime + 5, d_video.duration);
		/// -5s
		} else if (e.code == "ArrowLeft") {
			d_video.currentTime = Math.max(d_video.currentTime - 5, 0);
		/// UNZOOM
		} else if (e.key == "-") {
			d_unzoom.click();
		/// ZOOM
		} else if (e.key == "+") {
			d_zoom.click();
		/// ZOOM RESET
		} else if (e.key == "0") {
			d_reset_zoom.click();
		}
	});
	document.addEventListener("mousemove", function(e) {
		let		box;

		d_controls.style.opacity = "1";
		document.body.style.cursor = "auto";
		clearTimeout(controls_focused_timer);
		/// CHECK IF MOUSE IS ON CONTROLS
		box = d_controls.getBoundingClientRect();
		if (e.clientX < box.x || e.clientX > box.x + box.width
		|| e.clientY < box.y || e.clientY > box.y + box.height) {
			controls_focused_timer = setTimeout(function() {
				d_controls.style.opacity = "0";
				document.body.style.cursor = "none";
			}, 1000);
		}
	});
	document.addEventListener("click", function() {
		/// RESET FOCUS AFTER CLICK
		d_time_seek.blur();
		d_zoom.blur();
		d_unzoom.blur();
		d_reset_zoom.blur();
		d_play.blur();
		d_reverse.blur();
	});
}



//////////////////////////////////////////////////
/// ASCII
//////////////////////////////////////////////////

function	setup() {
	/// VARIABLES
	dark_mode = true;
	font_size = 100;
	/// SETUP BUTTONS
	setup_buttons();
	/// HANDLE CANVAS
	set_canvas_fit(CANVAS_COVER);
	create_canvas("player_video");
	ready = false;
	ascii_resized = false;
	/// HANDLE VIDEO
	d_video = document.createElement("video");
	d_video.autoplay = true;
	/// LOAD VIDEO
	document.getElementById("input_video").addEventListener("change", function (e) {
		let	input;
		let	reader;

		input = e.target;
		reader = new FileReader();
		reader.addEventListener("load", function () {
			input.style.display = "none";
			d_video.src = reader.result;
			d_video.load();
			d_video.loop = true;
			d_video.addEventListener("loadeddata", function () {
				ready = true;
				d_canvas = document.createElement("canvas");
				context = d_canvas.getContext("2d");
				d_time_duration.innerText = format_time(d_video.duration);
				d_time_seek.max = d_video.duration;
			}, false);
		});
		reader.readAsDataURL(input.files[0]);
	});
}

function	draw() {
	if (ready == true && d_video.videoWidth > 0) {
		/// UPDATE TIME
		d_time_elapsed.innerText = format_time(d_video.currentTime);
		d_time_seek.value = d_video.currentTime;
		/// INIT
		if (ascii_resized == false) {
			resize_canvas(d_video.videoWidth, d_video.videoHeight);
			/// COMPUTE DEFAULT FONT SIZE
			default_font_size = floor((document.body.clientHeight / (canvas_height * char_height)) * 100);
			font_size = default_font_size;
			d_video_player.style["font-size"] = font_size + "%";
			ascii_resized = true;
		}
		/// SHOT VIDEO
		d_canvas.width = canvas_width;
		d_canvas.height = canvas_height;
		context.drawImage(d_video, 0, 0, d_canvas.width, d_canvas.height);
		/// COMPUTE
		draw_characters();
	}
}
