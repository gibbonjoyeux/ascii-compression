# ascii-compression

Ecological and artistical video compression project which aims to reduce massively video weight (and ecological impact)
by using an ascii aesthetic, a text only rendering.

## PROJECT

This project is divided into two main sections, the **compressor** and the **reader**.

The **compressor** is a software that converts your videos into ascii compressed ones.
The **reader** is web page which allows you to read ascii compressed videos.

### READER

'reader/'

The reader is web oriented in a way that it can be integrated as you want on your own website.
The example let you select a compressed video from your computer and watch it as ascii images.

It is only made with **HTML**, **CSS** and **JAVASCRIPT** languages. No library is required exept **ascii.js**,
already integrated into the program, which is a really lightweight library made to build ascii games, animations,
user intefraces or generative sketches.

### COMPRESSER

'compresser/'

The compressor is made with **Processing** (An open source framework which allows you to easily build
games or graphic softwares) and use **ffmpeg** (An open source software which allows you to easily
create or convert videos).
