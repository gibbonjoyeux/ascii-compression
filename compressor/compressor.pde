/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

import	processing.video.*;
import	g4p_controls.*;
import	java.nio.file.Paths;
import	java.nio.file.Path;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

String		CHARS	= " `~._^|',-!:}+{=\\/*;[]7oc><i?)(rlt1jsIz3vCuJ%5aYn\"298e0f&L6OS$VGZxTyUhP4wkDFdgqbRpmX@QAEHK#BNWM";
GSlider		slider_width;
GSlider		slider_height;
GSlider		slider_font;
GButton		button_run;
GCheckbox	checkbox_preview;
GCheckbox	checkbox_contrast;
Movie		video;
PGraphics	canvas;
String		input_path;
String		output_path;
String		output_directory_path;
String		ffmpeg_path;
int			frame;
int			inactivity_count;
float		char_width;
float		char_height;
int			canvas_width;
int			canvas_height;
int			state;
float[][]	ascii_canvas;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// COMPRESSION
//////////////////////////////////////////////////

void	compress_frame() {
	int		pos;
	int		x, y;

	/// COMPRESS DATA TO CANVAS
	canvas.beginDraw();
	canvas.clear();
	canvas.loadPixels();
	for (y = 0; y < canvas_height; ++y) {
		for (x = 0; x < canvas_width; ++x) {
			pos = y * canvas.width + x;
			canvas.pixels[pos] = color(ascii_canvas[y][x]);
		}
	}
	canvas.updatePixels();
	canvas.save(String.format("%s/output/%06d.png", output_directory_path, frame));
	canvas.endDraw();
}

//////////////////////////////////////////////////
/// STATES
//////////////////////////////////////////////////

void	state_dimensions() {
	String	format;

	clear();
	textAlign(LEFT, TOP);
	/// DRAW ASCII PREVIEW
	if (checkbox_preview.isSelected()) {
		video.read();
		translate_to_ascii();
		preview_ascii();
	}
	/// DRAW DIMENSIONS
	textSize(20);
	fill(255, 255, 255, 200);
	format = String.format("%d x %d", canvas_width, canvas_height);
	rect(0, height - 60, 110, 60);
	fill(0);
	text(format, 0, height - 20);
	fill(255, 255, 255);
}

void	state_compression() {
	float	frame_duration;
	float	current_frame;

	/// HANDLE FRAME
	video.play();
	frame_duration = 1.0 / video.frameRate;
	current_frame = (frame + 0.5) * frame_duration;
	/// VIDEO ENDED
	if (current_frame >= video.duration()) {
		state = 3;
		return;
	}
	/// VIDEO PLAYING
	video.jump(current_frame);
	video.pause();
	if (video.available()) {
		clear();
		while (video.available()) {
			video.read();
		}
		translate_to_ascii();
		if (checkbox_preview.isSelected()) {
			preview_ascii();
		}
		compress_frame();
		image(canvas, width / 2 - canvas.width / 2, height / 2 - canvas.height / 2);
		++frame;
		inactivity_count = 0;
	} else {
		++inactivity_count;
		if (inactivity_count >= 5) {
			state = 3;
		}
	}
}

void	state_creation() {
	println("processing output");
	cmd_create_video();
	state = 4;
}

//////////////////////////////////////////////////
/// FFMPEG
//////////////////////////////////////////////////

void	ffmpeg_path_set(File selected) {
	ffmpeg_path = selected.getAbsolutePath();
	saveStrings("ffmpeg_path.txt", new String[] {ffmpeg_path});
	selectInput("Select a file to process:", "input_selected");
}

//////////////////////////////////////////////////
/// PROCESSING
//////////////////////////////////////////////////

void	setup() {
	String[]	path;

	state = 0;
	/// INIT WINDOW
	size(500, 500);
	surface.setResizable(true);
	textFont(createFont("Courier New", 16));
	/// SELECT FILE
	video = null;
	frame = 0;
	inactivity_count = 0;
	/// LOAD FFMPEG PATH
	path = loadStrings("ffmpeg_path.txt");
	if (path == null) {
		println("require ffmpeg path");
		selectInput("Select ffmpeg path:", "ffmpeg_path_set");
	} else {
		ffmpeg_path = path[0];
		selectInput("Select a file to process:", "input_selected");
	}
}

void	draw() {
	/// STATE: FORMAT
	if (state == 1) {
		state_dimensions();
	/// STATE: COMPRESSION
	} else if (state == 2) {
		state_compression();
	/// STATE: CREATE VIDEO
	} else if (state == 3) {
		state_creation();
	/// STATE: FINISHED
	} else if (state == 4) {
		exit();
	}
}
