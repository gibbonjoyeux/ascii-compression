/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

import	processing.video.*;
import	g4p_controls.*;
import	java.nio.file.Paths;
import	java.nio.file.Path;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void	setup_buttons() {
	/// FONT SIZE SLIDER
	slider_font = new GSlider(this, 0, 0, width, 50, 10);
	slider_font.setShowDecor(true, true, true, true);
	slider_font.setLimits(16, 1, 70);
	slider_font.setUnits("px");
	slider_font.setAlpha(200);
	slider_font.addEventHandler(this, "update_format_from_font");
	/// WIDTH SLIDER
	slider_width = new GSlider(this, 0, 50, width, 50, 10);
	slider_width.setShowDecor(true, true, true, true);
	slider_width.setLimits(10, 300);
	slider_width.setAlpha(200);
	slider_width.addEventHandler(this, "update_format_from_width");
	/// HEIGHT SLIDER
	slider_height = new GSlider(this, 0, 100, width, 50, 10);
	slider_height.setShowDecor(true, true, true, true);
	slider_height.setLimits(10, 300);
	slider_height.setAlpha(200);
	/// UPDATE SLIDERS VALUE
	slider_height.addEventHandler(this, "update_format_from_height");
	slider_font.setFocus(true);
	update_format_from_font(null, null);
	/// COMPRESS BUTTON
	button_run = new GButton(this, width / 2 - 50, height / 2 - 25, 100, 50, "COMPRESS");
	button_run.addEventHandler(this, "button_handler");
	/// PREVIEW CHECKBOX
	checkbox_preview = new GCheckbox(this, 0, height - 60, 100, 20, "preview");
	checkbox_preview.setTextBold();
	checkbox_preview.setSelected(true);
	/// CONTRAST CHECKBOX
	checkbox_contrast = new GCheckbox(this, 0, height - 40, 100, 20, "contrast");
	checkbox_contrast.setTextBold();
	checkbox_contrast.setSelected(true);
}

void	update_format_from_font(GSlider slider, GEvent event) {
	if (slider_font.hasFocus()) {
		textSize(slider_font.getValueI());
		char_width = textWidth('X');
		char_height = slider_font.getValueF();
		canvas_width = (int)((float)video.width / char_width);
		canvas_height = (int)((float)video.height / char_height);
		slider_height.setValue(canvas_height);
		slider_width.setValue(canvas_width);
		ascii_canvas = new float[canvas_height][canvas_width];
	}
}

void	update_format_from_width(GSlider slider, GEvent event) {
	float	char_width_before;

	if (slider_width.hasFocus()) {
		canvas_width = slider_width.getValueI();
		char_width_before = char_width;
		char_width = (float)video.width / canvas_width;
		char_height = (char_height * char_width) / char_width_before;
		canvas_height = (int)((float)video.height / char_height);
		slider_height.setValue(canvas_height);
		slider_font.setValue(char_height);
		ascii_canvas = new float[canvas_height][canvas_width];
	}
}

void	update_format_from_height(GSlider slider, GEvent event) {
	if (slider_height.hasFocus()) {
		canvas_height = slider_height.getValueI();
		char_height = (float)video.height / (float)canvas_height;
		textSize(char_height);	
		char_width = textWidth('X');
		canvas_width = (int)((float)video.width / char_width);
		slider_width.setValue(canvas_width);
		slider_font.setValue(char_height);
		ascii_canvas = new float[canvas_height][canvas_width];
	}
}

void	button_handler(GButton button, GEvent event) {
	/// CREATE CANVAS
	canvas = createGraphics(canvas_width, canvas_height);
	ascii_canvas = new float[canvas_height][canvas_width];
	/// REMOVE BUTTONS
	slider_font.setAlpha(0);
	slider_width.setAlpha(0);
	slider_height.setAlpha(0);
	button_run.setAlpha(0);
	/// STOP VIDEO
	video.stop();
	video.play();
	video.jump(0);
	video.volume(0);
	/// SELECT OUTPUT
	selectOutput("Select a file to write to:", "output_selected");
}
