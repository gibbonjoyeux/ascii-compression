/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

import	processing.video.*;
import	g4p_controls.*;
import	java.nio.file.Paths;
import	java.nio.file.Path;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void	input_selected(File selected) {
	if (selected == null) {
		exit();
	} else {
		/// INIT VIDEO
		input_path = selected.getAbsolutePath();
		video = new Movie(this, input_path);
		video.loop();
		video.jump(0);
		video.read();
		/// INIT FONT
		textFont(createFont("Courier New", 16));
		char_width = ceil(textWidth('X'));
		char_height = 16;
		canvas_width = (int)(video.width / char_width);
		canvas_height = (int)(video.height / char_height);
		ascii_canvas = new float[canvas_height][canvas_width];
		/// RESIZE WINDOW
		surface.setSize(video.width, video.height);
		setup_buttons();
		state = 1;
	}
}

void	output_selected(File selected) {
	Path	path;

	/// GET OUTPUT PATH
	output_path = selected.getAbsolutePath();
	path = Paths.get(output_path);
	output_directory_path = path.getParent().toString();
	println(selected);
	println(output_directory_path);
	println("output selected");
	/// RUN COMPRESSION
	state = 2;
}
