/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

import	processing.video.*;
import	g4p_controls.*;
import	java.nio.file.Paths;
import	java.nio.file.Path;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void	cmd_run(String[] cmd) {
	ProcessBuilder	process_builder;
	Process		process;
	int		return_code;

	/// BUILD PROCESS
	process_builder = new ProcessBuilder(cmd);
	try {
		/// RUN COMMAND
		process = process_builder.start();
		/// WAIT FOR COMMAND TO END
		return_code = process.waitFor();
		println("return:", return_code);
	} catch (Exception e) {
		println("FAILURE");
		e.printStackTrace();
	}
}

void	cmd_create_video() {
	String[]	args;

	args = new String[] {
		/// FFMPEG
		ffmpeg_path,
		/// INPUT -> video (from compressed frames)
		"-r", Float.toString(video.frameRate),	// original framerate
		"-i", String.format("%s/output/%%06d.png", output_directory_path),
		/// INPUT -> audio (from original video)
		"-i", input_path,
		/// OUTPUT -> video
		"-c:v", "libx264",						// video codec
		"-pix_fmt", "yuv420p",					// color channels format yuv
		"-crf", "23",							// video quality
		"-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2",	// video format
		"-map", "0:v",							// load video from input[0]
		/// OUTPUT -> audio
		"-c:a", "copy",							// use original video audio codec
		"-map", "1:a",							// load audio from input[1]
		"-y",									// overwrite output
		output_path
	};
	cmd_run(args);
}
