/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

import	processing.video.*;
import	g4p_controls.*;
import	java.nio.file.Paths;
import	java.nio.file.Path;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void	translate_to_ascii() {
	int		pos;
	float	gray;
	color	cell;
	int		x, y;
	int		j, k;
	float	min_gray, max_gray;
	int		merge_count;

	textSize(char_height);
	video.loadPixels();
	/// SWITCH TO GRAYSCALE
	min_gray = 255;
	max_gray = 0;
	/// FOR EACH CHARACTER
	for (y = 0; y < canvas_height; ++y) {
		for (x = 0; x < canvas_width; ++x) {
			merge_count = 0;
			gray = 0;
			/// FOR EACH PIXEL
			for (j = 0; j < char_height; ++j) {
				if (y * char_height + j >= video.height) {
					break;
				}
				for (k = 0; k < char_width; ++k) {
					if (x * char_width + k >= video.width) {
						break;
					}
					/// GET PIXEL GRAY VALUE
					pos = (int)(y * char_height + j) * video.width + (int)(x * char_width + k);
					cell = video.pixels[pos];
					gray += (red(cell) + green(cell) + blue(cell)) / 3.0;
					++merge_count;
				}
			}
			if (merge_count == 0) {
				pos = (int)(y * char_height) * video.width + (int)(x * char_width);
				cell = video.pixels[pos];
				gray = (red(cell) + green(cell) + blue(cell)) / 3.0;
			} else {
				gray /= merge_count;
			}
			ascii_canvas[y][x] = gray;
			min_gray = min(gray, min_gray);
			max_gray = max(gray, max_gray);
		}
	}
	if (checkbox_contrast.isSelected() == false) {
		min_gray = 0;
		max_gray = 255;
	}
	/// REMAP WHITE / BLACK BALANCE
	for (y = 0; y < canvas_height; ++y) {
		for (x = 0; x < canvas_width; ++x) {
			if (min_gray != max_gray) {
				gray = (int)map(ascii_canvas[y][x], min_gray, max_gray, 0, 255);
			} else {
				gray = min_gray;
			}
			ascii_canvas[y][x] = gray;
		}
	}
}

void	preview_ascii() {
	int	x, y;

	for (y = 0; y < canvas_height; ++y) {
		for (x = 0; x < canvas_width; ++x) {
			text(CHARS.charAt((int)map(ascii_canvas[y][x], 0, 255, 0, CHARS.length() - 1)), x * char_width, y * char_height);
		}
	}
}
